# Task 4

#### Reading

Read chapters 7 and 8 of K&R.  Also read chapter 15 in 
**Pointers on C** by Reek.  

Look up and understand the table on [asciitable](www.asciitable.com).

Read chapter 3 of Smart.  

Read the Network Protocols Handbook for HTTP (page 20).

#### Questions

1. What are various commands of HTTP?
2. What are the file descriptor numbers for standard in, out, and error?
3. Define a file (in C).
4. How would you generate a random number in C?
5. What functions are used to interact with files?
6. The Affine Cipher uses modulo 26 arithmetic.  The key is two values, a and b.  We get c = a*m + b (mod 26) for each letter.  What restrictions are on the key, and why?
7. How can the shift cipher be broken?
8. How can the substitution cipher be broken?
9. How can the Vigenere cipher be broken?
10. What are the most common letters in English?

#### Exercises

1. Reek, chapter 15, programming exercise 8.
2. Write an interactive "game" of your own design (for the terminal).
3. Write a program that can encode/decode a file to/from Base64.
4. Write a library (.c and corresponding .h file) for shift ciphers.
5. Write a library for the Vigenere cipher.
6. Write a program that can encrypt/decrypt files with any cipher in Smart chapter 3.
7. Write a library for frequency analysis.
8. Write a program that performs frequency analysis on the Vigenere cipher.  Test it on a file.
9. Write a program that brute forces the shift cipher.  Test it on a file.
10. Write part of a library to construct HTTP requests.  



