# Virtual Machine Setup

#### Step 1

Download and install Oracle's Virtualbox program.  

#### Step 2

Download all of the following files (let me know if links are broken):

[CentOS ISO File](http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso)

[Debian ISO File](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.9.0-amd64-netinst.iso)

[Arch ISO File](http://mirror.math.princeton.edu/pub/archlinux/iso/2019.04.01/archlinux-2019.04.01-x86_64.iso)

#### Step 3 - CentOS

Create a new VM in Virtualbox.

Type: Linux, and Version: Redhat 64-bit.

Change memory as you like - I prefer 4096MB or 8192MB depending on how much you have available.

Create a virtual hard disk now.  File type isn't important, VDI is preferred.

Dynamically allocated, give it 30 GB on the next screen.

Go into Settings, Storage, Controller:IDE, and the "Empty" disk symbol.

On the right is a picture of a disk with a dropdown arrow, select the 
"Choose Virtual Optical Disk File" option, and locate the downloaded 
CentOS .iso file from step 2.

Close settings and hit the start button.  

Follow the instructions given in the VM to set it up.  

DO NOT install a graphical user interface or desktop environment, it should
be a command line only machine.  

Let me know if you run into trouble on the OS install, also Google.

Log in, and as root run the following commands:

        dhclient -v enp0s3
        yum group install "Development Tools"
        yum install -y vim emacs git 

#### Step 4 - Debian

The procedure is mostly the same as above.

Use Version: Debian 64-bit instead of Redhat.

You'll use the Debian .iso file instead of the CentOS one.  

Again, DO NOT install a graphical user interface or desktop environment, 
you may need to uncheck those boxes on the select software screen.  

Log in, and as root run the following commands:

        apt install vim emacs gcc make automake autoconf git libtool

#### Step 5 - Arch 

The Arch setup process is complicated, and is a challenge in itself.  You are expected to crash and burn at least 3 times before getting it right.  The struggle will help you learn a lot about linux though!  Google a lot if you run into issues.  If you get it on your first try, congrats!  

Follow (loosely) the instructions [here](https://wiki.archlinux.org/index.php/Installation_guide)

Once you have an Arch VM that works, you are done!

Log in, and as root run the following commands:

        pacman -Syu
        pacman -S vim emacs gcc make git 

#### Step 6 

Ensure that your usernames/passwords are recorded somewhere, both
user and root credentials.  

Any further needed libraries, packages, or programs can be installed 
as needed!  

#### Setp 7

On each machine, clone your fork of this repository.  
