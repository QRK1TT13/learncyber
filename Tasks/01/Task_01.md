# Task 1

#### Setup (only once)

Log into your gitlab account and fork this repository (gitlab.com/QRK1TT13/CyberLearn).
This will allow you to access and modify your own copy of it.  I will be able 
to check your work through the forked repository without your work modifying 
this repository, thus others will be able to use it as well.  

Let me know if you have questions/issues with doing so.  

Also, set up a virtual machine, instructions are [here](VM.md)

#### Reading

Read from **The C Programming Language** chapters 1 and 2.  Most of this 
is pretty basic C programming, and shouldn't be too hard.  Try a couple 
of the demo programs, writing them yourself and compiling then running them.  

Additional info from Albert:

- Never use tabs (only use spaces - I use 8 of them to represent a tab)
- The book shows the "cc" command, we use "gcc" instead.
- Never use "for" loops.
- For now we are doing arrays on the stack with the "int ndigit[10];" syntax, but this is not good and we will change it soon.

Read also the man page for printf by typing the command "man printf" into 
the terminal of one of your VM's.  If man pages are not installed, install 
them and then read it.  

#### Questions

1. What filename does gcc produce by default?
2. What is the escape character for a new line?
3. #define and #include are examples of ___________ directives.
4. What is the difference between "++1;" and "1++;"?
5. Where are variables declared?
6. 0 is false, and 1 is true.  What is -1?
7. Do functions get passed a value, or a reference?
8. What is the syntax for a comment?
9. What are the relational and logical operators, and how does each work?
10. What are the bitwise operators, and how do they work?
11. How do you ensure that you have the correct order of evaluation of multiple operators?

#### Exercises

Do the following in C.  Give each problem its own file.  Comment 
your code well.

1. Book Exercise 1-1
2. Book Exercise 1-4
3. Book Exercise 1-6
4. Book Exercise 1-7
5. Book Exercise 1-10
6. Book Exercise 1-14
7. Book Exercise 1-19
8. Book Exercise 2-3
9. Book Exercise 2-7

Compile each with a command similar to:

        gcc -o ex_1 ex_1.c